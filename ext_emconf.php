<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "hive_facts".
 *
 * Auto generated 09-12-2021 16:54
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
    'title' => 'HIVE>Facts/Reasons',
    'description' => 'HIVE Facts & Reasons Content-Element',
    'category' => 'fe',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'author_company' => 'teufels GmbH',
    'state' => 'stable',
    'version' => '2.5.2',
    'constraints' =>
      array (
        'depends' =>
        array (
          'typo3' => '10.4.0-0.0.0',
        ),
        'conflicts' =>
        array (
        ),
        'suggests' =>
        array (
        ),
      ),
);

