<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

call_user_func(function () {

    /**
     * Register icons
     */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'tx_hivefacts_facts',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => 'EXT:hive_facts/Resources/Public/Icons/Extension.svg',
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_facts/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig">'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_facts/Configuration/TsConfig/Page/BackendPreview.tsconfig">'
    );
    // Add backend preview hook
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['hive_facts'] =
        HIVE\HiveFacts\Hooks\PageLayoutViewDrawItem::class;
});

