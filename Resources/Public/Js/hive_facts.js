$( document ).ready( function() {

    gsap.registerPlugin(ScrollTrigger);

    $( ".tx_hive_facts .hive-facts-item.fact .value" ).each(function( index ) {

        // Initialize Configuration
        var $value = parseFloat($( this ).data("value"));
        var $duration =  parseInt($( this ).data("duration"));
        if( $duration == 0) { $duration =  parseInt($( this ).data("duration-global")); }

        var $factIdentifier = $(this).attr('id');

        const options = {
            startVal: parseFloat($( this ).data("start-value")),
            decimalPlaces:  parseInt($( this ).data("decimalplaces")),
            duration: $duration,
            useGrouping: Boolean($( this ).data("usegrouping")),
            separator: $( this ).data("separator"),
            decimal: $( this ).data("decimal"),
            prefix: $( this ).data("prefix"),
            suffix: $( this ).data("suffix"),
        };

       var countUpElement = new countUp.CountUp($factIdentifier, $value, options);

        gsap.to($( this ), {
            scrollTrigger: {
                trigger: $( this ),
                start: "top bottom",
                end: "bottom top",
                once: true,
                onEnter: () => {
                    if (!countUpElement.error) {
                        countUpElement.start();
                    } else {
                        console.error(countUpElement.error);
                    }
                }
            }
        });

    });

});