<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

call_user_func(function () {

//Adding Custom CType Item Group
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItemGroup(
    'tt_content',
    'CType',
    'hive',
    'HIVE',
    'after:special'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['hivefacts_facts'] = 'tx_hivefacts_facts';
$tempColumns = [
    'tx_hivefacts_backendtitle' => [
        'config' => [
            'autocomplete' => '0',
            'behaviour' => [
                'allowLanguageSynchronization' => false,
            ],
            'type' => 'input',
        ],
        'description' => 'HIVE Facts & Reasons Content-Element',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivefacts_backendtitle',
    ],
    'tx_hivefacts_duration' => [
        'config' => [
            'autocomplete' => '0',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
            'default' => '2',
            'eval' => 'int',
            'type' => 'input',
        ],
        'description' => 'global animation duration in seconds (if not set in Element)',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivefacts_duration',
    ],
    'tx_hivefacts_element' => [
        'config' => [
            'appearance' => [
                'collapseAll' => '1',
                'enabledControls' => [
                    'dragdrop' => '1',
                ],
                'levelLinksPosition' => 'both',
                'useSortable' => '1',
                'expandSingle' => '0',
                'showAllLocalizationLink' => '1',
                'showPossibleLocalizationRecords' => '1',
                'showRemovedLocalizationRecords' => '1',
                'showSynchronizationLink' => '0',
            ],
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_hivefacts_element',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
            'minitems' => 1,
        ],
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivefacts_element',
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tt_content.CType.hivefacts_facts',
    'hivefacts_facts',
    // Icon
    'tx_hivefacts_facts',
    // The group ID, if not given, falls back to "none" or the last used --div-- in the item array
    'hive'
];
$tempTypes = [
    'hivefacts_facts' => [
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_hivefacts_backendtitle,tx_hivefacts_duration,tx_hivefacts_element,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames, --palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
    ],
];
$GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

});

