<?php
return [
    'ctrl' => [
        'sortby' => 'sorting',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'editlock' => 'editlock',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'translationSource' => 'l10n_source',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'title' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element',
        'label' => 'tx_hivefacts_element_backendtitle',
        'iconfile' => 'EXT:hive_facts/Resources/Public/Icons/Extension.svg',
        'hideTable' => true,
        'type' => 'record_type',
    ],
    'palettes' => [
        'language' => [
            'showitem' => '
                        sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l18n_parent
                    ',
        ],
        'hidden' => [
            'showitem' => '
                        hidden;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:field.default.hidden
                    ',
        ],
        'access' => [
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access',
            'showitem' => '
                        starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
                        endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
                        --linebreak--,
                        fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:fe_group_formlabel,
                        --linebreak--,editlock
                    ',
        ],
    ],
    'columns' => [
        'record_type' => [
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tt_content.record_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivefacts_type.I.0',
                        '0',
                    ],
                    [
                        'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivefacts_type.I.1',
                        'reason',
                    ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'editlock' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:editlock',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        '',
                        '',
                    ],
                ],
            ],
        ],
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple',
                    ],
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0,
                    ],
                ],
                'default' => 0,
                'foreign_table' => 'tx_hivefacts_element',
                'foreign_table_where' => 'AND tx_hivefacts_element.pid=###CURRENT_PID### AND tx_hivefacts_element.sys_language_uid IN (-1, 0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true,
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => 2145913200,
                ],
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
        ],
        'fe_group' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'size' => 5,
                'maxitems' => 20,
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hide_at_login',
                        -1,
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.any_login',
                        -2,
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.usergroups',
                        '--div--',
                    ],
                ],
                'exclusiveKeys' => '-1,-2',
                'foreign_table' => 'fe_groups',
            ],
        ],
        'parentid' => [
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0,
                    ],
                ],
                'default' => 0,
                'foreign_table' => 'tt_content',
                'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.sys_language_uid IN (-1, ###REC_FIELD_sys_language_uid###)',
            ],
        ],
        'parenttable' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'sorting' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'tx_hivefacts_element_backendtitle' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
                'type' => 'input',
            ],
            'description' => 'Backendtitle of the Element',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_backendtitle',
        ],
        'tx_hivefacts_element_decimal' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'default' => ',',
                'type' => 'input',
            ],
            'description' => 'decimal char (for example \',\')',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_decimal',
        ],
        'tx_hivefacts_element_decimalplaces' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'default' => '0',
                'eval' => 'int',
                'type' => 'input',
            ],
            'description' => 'number of decimal places',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_decimalplaces',
        ],
        'tx_hivefacts_element_duration' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'eval' => 'int',
                'type' => 'input',
            ],
            'description' => 'animation duration in seconds',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_duration',
        ],
        'tx_hivefacts_element_grouping' => [
            'config' => [
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'renderType' => 'checkboxToggle',
                'type' => 'check',
            ],
            'description' => 'use grouping (thousends seperator) 
example: 1,000 vs 1000 ',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_grouping',
        ],
        'tx_hivefacts_element_image' => [
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'tx_hivefacts_element_image',
                ],
                'foreign_label' => 'uid_local',
                'foreign_selector' => 'uid_local',
                'overrideChildTca' => [
                    'columns' => [
                        'uid_local' => [
                            'config' => [
                                'appearance' => [
                                    'elementBrowserType' => 'file',
                                    'elementBrowserAllowed' => 'gif,jpg,jpeg,tif,tiff,bmp,pcx,tga,png,pdf,ai,svg',
                                ],
                            ],
                        ],
                    ],
                    'types' => [
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;audioOverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;videoOverlayPalette,
                                --palette--;;filePalette',
                        ],
                        [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                    ],
                ],
                'filter' => [
                    [
                        'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                    ],
                ],
                'appearance' => [
                    'useSortable' => '0',
                    'headerThumbnail' => [
                        'field' => 'uid_local',
                        'height' => '45m',
                    ],
                    'enabledControls' => [
                        'info' => true,
                        'new' => false,
                        'dragdrop' => true,
                        'sort' => false,
                        'hide' => true,
                        'delete' => true,
                    ],
                    'collapseAll' => '1',
                    'expandSingle' => '0',
                    'fileUploadAllowed' => '1',
                    'showAllLocalizationLink' => '1',
                    'showPossibleLocalizationRecords' => '1',
                    'showRemovedLocalizationRecords' => '1',
                    'showSynchronizationLink' => '0',
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'maxitems' => '1',
            ],
            'description' => 'Image or Icon of the Element',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_image',
        ],
        'tx_hivefacts_element_prefix' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'type' => 'input',
            ],
            'description' => 'text prepended to result',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_prefix',
        ],
        'tx_hivefacts_element_seperator' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'default' => '.',
                'type' => 'input',
            ],
            'description' => 'grouping separator char (for example \'.\' thousends seperator)',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_seperator',
        ],
        'tx_hivefacts_element_startvalue' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'eval' => 'double2',
                'type' => 'input',
            ],
            'description' => 'number to start at (use point as seperator)',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_startvalue',
        ],
        'tx_hivefacts_element_subtitle' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'type' => 'input',
            ],
            'description' => 'Subtitle of the Element',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_subtitle',
        ],
        'tx_hivefacts_element_suffix' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'type' => 'input',
            ],
            'description' => 'text appended to result',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_suffix',
        ],
        'tx_hivefacts_element_text' => [
            'config' => [
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'enableRichtext' => '1',
                'type' => 'text',
                'softref' => 'typolink_tag,images,email[subst],url',
            ],
            'description' => 'Text of the Element',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_text',
        ],
        'tx_hivefacts_element_title' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'type' => 'input',
            ],
            'description' => 'Title of the Element',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_title',
        ],
        'tx_hivefacts_element_value' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'eval' => 'double2',
                'type' => 'input',
            ],
            'description' => 'the value you want to arrive at (use point as seperator)',
            'exclude' => '1',
            'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_value',
        ],
        't3_origuid' => [
            'config' => [
                'type' => 'passthrough',
                'default' => 0,
            ],
        ],
        'l10n_source' => [
            'config' => [
                'type' => 'passthrough',
                'default' => 0,
            ],
        ],
    ],
    'types' => [
        1 => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,record_type,tx_hivefacts_element_backendtitle,tx_hivefacts_element_title,tx_hivefacts_element_subtitle,tx_hivefacts_element_text,tx_hivefacts_element_image,tx_hivefacts_element_startvalue,tx_hivefacts_element_value,tx_hivefacts_element_decimalplaces,tx_hivefacts_element_duration,tx_hivefacts_element_grouping,tx_hivefacts_element_seperator,tx_hivefacts_element_decimal,tx_hivefacts_element_prefix,tx_hivefacts_element_suffix,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;;access',
        ],
        'reason' => [
            'columnsOverrides' => [
                'tx_hivefacts_element_value' => [
                    'config' => [
                        'eval' => 'int',
                        'type' => 'input',
                    ],
                    'description' => 'number for enumerations',
                    'label' => 'LLL:EXT:hive_facts/Resources/Private/Language/locallang_db.xlf:tx_hivefacts_element.tx_hivefacts_element_value.number',
                ],
            ],
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,record_type,tx_hivefacts_element_backendtitle,tx_hivefacts_element_title,tx_hivefacts_element_subtitle,tx_hivefacts_element_text,tx_hivefacts_element_image,tx_hivefacts_element_value,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;;access',
        ],
    ],
];