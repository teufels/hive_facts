![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__facts-blue.svg)
![version](https://img.shields.io/badge/version-2.5.*-yellow.svg?style=flat-square)

HIVE > Facts/Reasons
==========
Facts & Reasons Content-Element based on CountUp.js

#### This version supports TYPO3

![TYPO3Version](https://img.shields.io/badge/10_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_facts`

## CountUp.js Documents
* [Starting Guide](https://inorganik.github.io/countUp.js/)
* [GitHub](https://github.com/inorganik/CountUp.js)
 
### Requirements
`CountUp.js: >=2.0.8 (UMD)`

### How to use
- Install with composer
- Import Static Template (before hive_thm_custom)
- make own Layout by override Partials (and if needed Theme) in hive_thm_custom

### Migration
- Migration from 1.* => if used type "Reason" on Plugin Settings. Type has to be set again on Element. 

### Notice
- developed with mask & mask_export

### Changelog
- 2.5.2 Language Behavior
- 2.5.0 supports Typo3 12 LTS
- 2.4.4 remove unused mask config
- 2.4.2 prepare for Typo3 12 LTS
- 2.4.0 implement 'HIVE' CType Group
- 2.3.0 add value field to reasons for number for enumerations
- 2.2.0 keep data_item in assign to partial for update compatibility - may remove on TYPO3 12
- 2.1.0 rendering Image
- 2.0.0 implement record_type for change fields based on selection 'fact','reason'
- 1.1.0 include JS/SCSS self (nogulp needed)
- 1.0.0 intial