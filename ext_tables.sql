CREATE TABLE tt_content (
    tx_hivefacts_backendtitle tinytext,
    tx_hivefacts_duration tinytext,
    tx_hivefacts_element int(11) unsigned DEFAULT '0' NOT NULL
);
CREATE TABLE tx_hivefacts_element (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    record_type varchar(100) NOT NULL DEFAULT '0',
    tx_hivefacts_element_backendtitle tinytext,
    tx_hivefacts_element_decimal tinytext,
    tx_hivefacts_element_decimalplaces tinytext,
    tx_hivefacts_element_duration tinytext,
    tx_hivefacts_element_grouping int(11) DEFAULT '0' NOT NULL,
    tx_hivefacts_element_image int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hivefacts_element_prefix tinytext,
    tx_hivefacts_element_seperator tinytext,
    tx_hivefacts_element_startvalue tinytext,
    tx_hivefacts_element_subtitle tinytext,
    tx_hivefacts_element_suffix tinytext,
    tx_hivefacts_element_text mediumtext,
    tx_hivefacts_element_title tinytext,
    tx_hivefacts_element_value tinytext,
    KEY language (l10n_parent,sys_language_uid)
);
